# jittershapes

Evoluted jitternodes

My evolution of Jiiternodes that comes with Inkscape
The original jiggles individual nodes in a path by some randomly distributed value

This version considers path not as a set of individual nodes but handles paths as shapes, trying to match them with other nodes in its vincinity
- The begin and end node of a path are considered to be the same if they are close enough.
- Nodes in different paths that are close enough are considered to be the same as well.
Close enough is defined using a parametered distance allowed between nodes.

Nodes' handles can be jiggled, with the same features as shapes.
