#!/usr/bin/env python
'''
Copyright (C) 2012 Juan Pablo Carbajal ajuanpi-dev@gmail.com
Copyright (C) 2005 Aaron Spike, aaron@ekips.org

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
import random, math, inkex
from inkex import paths


LEFT = 0
NODE = 1
RIGHT = 2
X = 0
Y = 1
# TODO
# N.B. No overlap
# Check effet locked layes
# /!\ Shapes of 2 shapes ? Probably no pb
# dist_* => null

def randomize(rx, ry, dist):
    if dist == "Gaussian" or dist == JitterShapes.dist_gaussian:
        r1 = random.gauss(0.0,rx)
        r2 = random.gauss(0.0,ry)
    elif dist == "Pareto" or dist == JitterShapes.dist_pareto:
        '''
        sign is used ot fake a double sided pareto distribution.
        for parameter value between 1 and 2 the distribution has infinite variance
        I truncate the distribution to a high value and then normalize it.
        The idea is to get spiky distributions, any distribution with long-tails is
        good (ideal would be Levy distribution).
        '''
        sign = random.uniform(-1.0,1.0)

        r1 = min(random.paretovariate(1.0), 20.0)/20.0
        r2 = min(random.paretovariate(1.0), 20.0)/20.0

        r1 = rx * math.copysign(r1, sign)
        r2 = ry * math.copysign(r2, sign)
    elif dist == "Lognorm" or dist == JitterShapes.dist_lognorm:
        sign = random.uniform(-1.0,1.0)
        r1 = rx * math.copysign(random.lognormvariate(0.0,1.0)/3.5,sign)
        r2 = ry * math.copysign(random.lognormvariate(0.0,1.0)/3.5,sign)
    elif dist == "Uniform" or dist == JitterShapes.dist_uniform:
        r1 = random.uniform(-rx,rx)
        r2 = random.uniform(-ry,ry)

    return [r1, r2]

def display(csp, tag=None):
    indent = ""
    if tag:
        inkex.utils.debug(tag + ":")
        indent = "    "
    else:
        inkex.utils.debug('')
    for i in LEFT, NODE, RIGHT:
        inkex.utils.debug('{}csp[{}] = [{},{}]'.format(indent, i, csp[i][X], csp[i][Y]))

def idNode( node ):
    # Identify the NODE
    return 'node[{},{}]'.format( node[ X ], node[ Y ] )

def idHandle( prev, node ):
    # Identify the handle
    return [ 'handle[{}-{}]'.format( idNode( prev ), idNode( node )),
             'handle[{}-{}]'.format( idNode( node ), idNode( prev )) ]

def snap( csp, snap ):
    # Snap NODE
    if snap == 0:
            return csp

    # Snap NODE to a multiple of snap
    # Shift the handles if the same direction
    shift = [ csp[ NODE ][ X ] - round( csp[ NODE ][ X ] / snap ) * snap,
              csp[ NODE ][ Y ] - round( csp[ NODE ][ Y ] / snap ) * snap ]

    for i in LEFT, NODE, RIGHT:
        for xy in X, Y:
            csp[ i ][ xy ] -= shift[ xy ]
    return csp

class JitterShapes(inkex.EffectExtension):
    '''Jiggle shapes around'''
    def add_arguments(self, pars):
        pars.add_argument("--radiusx",
                        type=float, default=10.0,
                        help="Randomly move nodes and handles within this radius, X (px)")
        pars.add_argument("--radiusy",
                        type=float, default=10.0,
                        help="Randomly move nodes and handles within this radius, Y (px)")
        pars.add_argument("--delta",
                        type=float, default=3.0,
                        help="Snap nodes to multiples of, X and Y (px)")
        pars.add_argument("--handles",
                        type=inkex.Boolean, default=True,
                        help="Randomize shapes' handles")
        pars.add_argument("--shapes",
                        type=inkex.Boolean, default=True,
                        help="Randomize shapes")
        pars.add_argument("--debug",
                        type=inkex.Boolean, default=False,
                        help="Debug mode. Ends in i=1/0<br>JZeroDivisionError: division by zero")
        pars.add_argument("--dist",
                        type=self.arg_method('dist'), default=randomize,
                        help="Distribution of displacement")
        pars.add_argument("--tab",
                        help="The selected UI-tab when OK was pressed")

    def effect(self):
        deltas = {}
        jitter = {}
        # for id, node in self.selected.iteritems():
        # d.iteritems() -> iter(d.items())
        for id, node in iter(self.svg.selected.items()):
            if node.tag == inkex.addNS('path','svg'):
                d = node.get('d')
                if self.options.debug:
                    inkex.utils.debug('\nnode: ' + str(node.attrib["id"])) # path9999
                # parsePath:: Parse SVG path and return an array of segments.
                # Removes all shorthand notation.
                # Converts coordinates to absolute. <==
                # p = cubicsuperpath.parsePath(d)
                p = paths.CubicSuperPath(paths.Path(d))
                for subpath in p:
                    prev = None
                    for csp in subpath:
                        if self.options.debug:
                            display( csp, "beginning" )
                        if self.options.delta > 0:
                            # Snap NODE
                            csp = snap( csp, self.options.delta )

                        if self.options.debug:
                            display( csp, "snapped" )

                        # Identify NODE, apres Snap, avant Shift
                        idNODE = idNode( csp[ NODE ])
                        if self.options.debug:
                            inkex.utils.debug('idNODE = {}'.format( idNODE ))

                        if self.options.shapes:
                            # Shift NODE
                            if idNODE not in deltas:
                                # First encounter
                                deltas[ idNODE ] = randomize( self.options.radiusx,
                                    self.options.radiusy,
                                    self.options.dist )

                            if self.options.debug:
                                inkex.utils.debug('deltas[{}] = {}'.format( idNODE, deltas[ idNODE]))
                            # Shift NODE together with LEFT and RIGHT handles
                            for i in LEFT, NODE, RIGHT:
                                for xy in X, Y:
                                    csp[ i ][ xy ] += deltas[ idNODE ][ xy ]
                            if self.options.debug:
                                display( csp, "moved" )

                        if self.options.handles:
                            # Handles are on both sides of a segment (bewteen two nodes)
                            # Therefor handle jitters must be identified by the segment
                            # of two nodes, instead of by the node
                            # First and last NODE dont't have a LEFT and RIGHT handle
                            # respectively: It's value is identical to the NODE's

                            # Shift handles LEFT and RIGHT
                            if not prev:
                                # First node
                                prev = csp
                                continue
                            idHANDLE = None
                            if self.options.debug:
                                inkex.utils.debug('idHANDLE = {}'.format( idHANDLE ))
                            handles = idHandle( prev[ NODE ], csp[ NODE ] )
                            for h in handles:
                                if h in jitter:
                                    idHANDLE = h
                            if idHANDLE == None:
                                idHANDLE = handles[ 0 ]
                                jitter[ idHANDLE ] = [
                                    randomize( self.options.radiusx,
                                               self.options.radiusy,
                                               self.options.dist ),
                                    None,
                                    randomize( self.options.radiusx,
                                               self.options.radiusy,
                                               self.options.dist )]
                            if self.options.debug:
                                inkex.utils.debug('jitter[{}] = {}'.format( idHANDLE, jitter[  idHANDLE ]))
                            for xy in X, Y:
                                if idHANDLE == handles[ 0 ]:
                                    prev[ RIGHT ][ xy ] += jitter[ idHANDLE ][ RIGHT ][ xy ]
                                    csp[ LEFT ][ xy ] += jitter[ idHANDLE ][ LEFT ][ xy ]
                                else:
                                    # handles[ 1 ]
                                    prev[ RIGHT ][ xy ] += jitter[ idHANDLE ][ LEFT ][ xy ]
                                    csp[ LEFT ][ xy ] += jitter[ idHANDLE ][ RIGHT ][ xy ]
                            if self.options.debug:
                                display( csp, "handles moved" )

                        prev = csp

                    if self.options.debug:
                        for csp in subpath:
                            display( csp, "done" )
                # node.set('d',cubicsuperpath.formatPath(p))
                # node.set('d', str(paths.Path(unCubicSuperPath(p))))
                node.set('d', str(paths.Path(paths.CubicSuperPath(p).to_path().to_arrays())))

        for id, node in iter(self.svg.selected.items()):
            if node.tag == inkex.addNS('path','svg'):
                d = node.get('d')
                p = paths.CubicSuperPath(paths.Path(d))
                for subpath in p:
                    for csp in subpath:
                        display( csp, "final" )

        if self.options.debug:
            i=1/0

    def randomize(self, pos):
        """Randomise the given position [x, y] as set in the options"""
        delta = self.options.dist(self.options.radiusx, self.options.radiusy)
        return [pos[0] + delta[0], pos[1] + delta[1]]

    @staticmethod
    def dist_gaussian(x, y):
        """Gaussian distribution"""
        return random.gauss(0.0, x), random.gauss(0.0, y)

    @staticmethod
    def dist_pareto(x, y):
        """Pareto distribution"""
        # sign is used to fake a double sided pareto distribution.
        # for parameter value between 1 and 2 the distribution has infinite variance
        # I truncate the distribution to a high value and then normalize it.
        # The idea is to get spiky distributions, any distribution with long-tails is
        # good (ideal would be Levy distribution).
        sign = random.uniform(-1.0, 1.0)
        return x * math.copysign(min(random.paretovariate(1.0), 20.0) / 20.0, sign),\
               y * math.copysign(min(random.paretovariate(1.0), 20.0) / 20.0, sign)

    @staticmethod
    def dist_lognorm(x, y):
        """Log Norm distribution"""
        sign = random.uniform(-1.0, 1.0)
        return x * math.copysign(random.lognormvariate(0.0, 1.0) / 3.5, sign),\
               y * math.copysign(random.lognormvariate(0.0, 1.0) / 3.5, sign)

    @staticmethod
    def dist_uniform(x, y):
        """Uniform distribution"""
        # return random.uniform(-x, x), random.uniform(-y, y)

if __name__ == '__main__':
    JitterShapes().run()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99 guifont=Fixedsys:h11 guicursor=n:blinkwait0 guicursor=i:blinkwait0 guicursor=v:blinkwait0
